EESchema-DOCLIB  Version 2.0
#
$CMP ALM2403-Q1
D OpAmp, 450 mA, Output Current, Automotive, Low-Distortion, Dual-Channel, Integrated Protection for Resolver Drive
K opamp opv
F https://www.ti.com/lit/ds/symlink/alm2403-q1.pdf
$ENDCMP
#
$CMP DAC124S085
D 12-Bit, Micro Power, Quad Digital-to-Analog Converter With Rail-to-Rail Output, 012585
K dac 012585
F https://www.ti.com/lit/ds/symlink/dac124s085.pdf
$ENDCMP
#
$CMP INA219
D Zero-Drift, Bidirectional Current/Power Monitor (0-26V) With I2C Interface, SOT-23-8
K ADC I2C 16-Bit Oversampling Current Shunt
F http://www.ti.com/lit/ds/symlink/ina219.pdf
$ENDCMP
#
$CMP LMS4684
D 0.5 Ω, Low-voltage dual SPDT analog switch
F https://www.ti.com/lit/ds/symlink/lms4684.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1594365667707
$ENDCMP
#
$CMP LMX2594
D 15 GHz wideband PLLATINUM RF synthesizer
F https://www.ti.com/lit/ds/symlink/lmx2594.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1594365740000
$ENDCMP
#
$CMP OPA698
D Unity-Gain Stable, Wideband Voltage Limiting Amplifier, 250MHz GBWP, 450 MHz -3dB bandwidth
K opamp opa698 opv
F https://www.ti.com/lit/ds/symlink/opa698.pdf
$ENDCMP
#
$CMP OPA818
D  2.7 GHz, High-Voltage, FET-Input, Low Noise, Operational Amplifier, WSON-8, 036104
K opamp VFA 036104
F https://www.ti.com/lit/ds/symlink/opa818.pdf
$ENDCMP
#
$CMP OPA855xDSG
D 8-GHz Gain Bandwidth Product, Gain of 7-V/V Stable, Bipolar Input Amplifier, WSON-8
K opamp VFA
F http://www.ti.com/lit/ds/symlink/opa855.pdf
$ENDCMP
#
$CMP OPA856
D 1.1-GHz Unity-Gain Bandwidth, 0.9 nV /√Hz, Bipolar Input Amplifier, 036097
K opamp opa856 opv 036097
F https://www.ti.com/lit/ds/symlink/opa856.pdf
$ENDCMP
#
$CMP OPA857
D Ultralow-Noise, Wideband, Selectable-Feedback Resistance Transimpedance Amplifier, VQFN-16, 036101
K opamp opa857 tia transimpedance amplfier 036101
F https://www.ti.com/lit/ds/symlink/opa857.pdf
$ENDCMP
#
$CMP OPA858
D 5.5-GHz Gain Bandwidth Product, Gain of 7 V/V Stable, FET Input Amplifier, WSON-8, 036109
K opamp VFA 036109
F http://www.ti.com/lit/ds/symlink/opa858.pdf
$ENDCMP
#
$CMP OPA859xDSG
D 1.8 GHz Unity-Gain Bandwidth FET Input Amplifier, WSON-8
K opamp VFA
F http://www.ti.com/lit/ds/symlink/opa859.pdf
$ENDCMP
#
$CMP SN74LVC125A
D Buffer, Non-Inverting 4 Element 1 Bit per Element 3-State Output
F https://www.ti.com/lit/ds/symlink/sn74lvc125a.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1594365848489
$ENDCMP
#
$CMP SN74LVC157ADR
D Quadruple 4 Bit multiplexer, 1.65V to 3.6V
F https://www.ti.com/lit/ds/symlink/sn74lvc157a.pdf
$ENDCMP
#
$CMP SN74LVC157APWR
D Quadruple 4 Bit multiplexer, 1.65V to 3.6V
F https://www.ti.com/lit/ds/symlink/sn74lvc157a.pdf
$ENDCMP
#
$CMP THS4520
D Fully Differential OpAmp, 620 MHz, wideband, low noise, low distortion, rail-to-rail output, power down
K fda opamp
F https://www.ti.com/lit/ds/symlink/ths4520.pdf
$ENDCMP
#
$CMP TLV71333
D Low-Dropout Regulator, Capacitor-Free, 150mA
K ldo
F https://www.ti.com/lit/ds/symlink/tlv713p-q1.pdf
$ENDCMP
#
$CMP TLV9151
D 4.5-MHz, Rail-to-Rail Input/Output, Low Offset Voltage, Low Noise Op Amp, TLV9151IDBV
K opamp opv rrio
F https://www.ti.com/lit/ds/symlink/tlv9152.pdf
$ENDCMP
#
$CMP TPS63710
F https://www.ti.com/lit/ds/symlink/tps63710.pdf?ts=1601455739737&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTPS63710
$ENDCMP
#
$CMP TPS72301
D TPS72301, 200 mA, Adjustable, Low-Noise, High-PSRR, Negative Output, Low-Dropout Linear Regulator
K ldo
F https://www.ti.com/lit/ds/symlink/tps723.pdf
$ENDCMP
#
$CMP TPS7A19
D 40V, 450mA, Wide VIN, Low IQ, Low-Dropout, Voltage Regulator with Power Good
K LDO ldo linearregler regulator
F https://www.ti.com/lit/ds/symlink/tps7a19.pdf
$ENDCMP
#
$CMP TPS7A26
D 500mA,18-V, Ultra-LowIQ, Low-Dropout, Linear Voltage Regulator, Power-Good
K ldo
F https://www.ti.com/lit/ds/symlink/tps7a26.pdf
$ENDCMP
#
$CMP TPS7A53
D 3A, High-Accuracy, Low-Noise LDO Voltage Regulator
K ldo
F https://www.ti.com/lit/ds/symlink/tps7a53.pdf
$ENDCMP
#
$CMP TPS7A91
D 1A, High-Accuracy, Low-Noise LDO Voltage Regulator
K LDO ldo
F https://www.ti.com/lit/ds/symlink/tps7a91.pdf
$ENDCMP
#
$CMP TPSM53602
D 36-V Input, 2-A Power Module in Enhanced HotRod™ QFN Package
F https://www.ti.com/lit/ds/symlink/tpsm53602.pdf?ts=1603896672490&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTPSM53602
$ENDCMP
#
#End Doc Library
