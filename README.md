# KT_KiCad

### Template Installation
Place the KT folder in the KiCad user template directory. OS dependent, the path can be found by opening KiCad and going to **Preferences >> Configure Paths** and looking at KICAD_USER_TEMPLATE_DIR.

Use the template when creating a new project to automatically get all the libraries and footprints pulled into the new project.

#### Better Linking of Template Directory:
Under linux, the user template directory should be /home/kicad/template. Create the folder /home/kicad and then use  
`ln -s /path/to/kt_kicad/template /home/kicad`  
to keep the template directory in sync with the git repo. A similar process can be done for windows.

### Setting up KiCad Libraries
You need to define the variable "KT_LIBS" within KiCad. Do this by selecting **Preferences >> Configure Paths** and adding it. This should point to your "kt_kicad" directory.

### Recommended Action Plugins

Installation: Copy folder into the C:\Program Files\KiCad\share\kicad\scripting\plugins folder
Under Linux: ~/.kicad/scripting or /usr/share/kicad/scripting

- Trace Clearance (Action Plugin) [Github](https://github.com/easyw/RF-tools-KiCAD) (needs wxpython, numpy)
- Rounder for tracks (Action Plugin) [Github](https://github.com/easyw/RF-tools-KiCAD)
- Solder Mask Expander (Action Plugin) [Github](https://github.com/easyw/RF-tools-KiCAD)
- Track Length (Action Plugin) [Github](https://github.com/easyw/RF-tools-KiCAD)
- Via Fence Generator (Action Plugin) [pyclipper required] (pip install pyclipper) [Github](https://github.com/easyw/RF-tools-KiCAD)
- Teardrops (Action Plugin) [Github](https://github.com/NilujePerchut/kicad_scripts)
- Via Stiching (Action Plugin) [Github](https://github.com/jsreynaud/kicad-action-scripts)
